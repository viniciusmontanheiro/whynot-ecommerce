//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 100) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
        $(".logo-menu-hide").addClass("logo-menu-show");

        $(".layer").addClass("hide-layer");
        $("#nav-header").addClass("set-shadow");

        if ($(".navbar").offset().top > 400) {
            $("#carousel").addClass("blur");
        }else{
            $("#carousel").removeClass("blur");

        }

    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
        $(".logo").removeClass("set-logo-size");
        $(".layer").removeClass("hide-layer");
        $(".logo-menu-hide").removeClass("logo-menu-show");
        $("#nav-header").removeClass("set-shadow");



    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $.material.init();
    $.material.ripples();
    $.material.input();
    $.material.checkbox();
    $.material.radio();

    $( ".draggable" ).draggable();

    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });


    var count = 0;
    var item = $( ".add" );
    item.click(function(){
        var self = $(this);
        var produto = self.closest('.thumbnail')[0];
        $(produto).effect( 'transfer',{ to: "#carrinho", className: "ui-effects-transfer" }, 500, callback(produto));
        return false;
    });

    function callback() {
        count++;
        adiciona(count);
    };

    var adiciona = function(valor){
        if(valor >= 1){
            $('.carrinho').hide();
            $('.badge').html(valor).fadeIn(800);
        }


    }

});
